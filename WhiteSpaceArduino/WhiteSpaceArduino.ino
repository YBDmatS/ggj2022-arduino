#include "LedControl.h"

LedControl lc = LedControl(32, 30, 31, 1);

int blueLed_PIN = 4;
int blueButton_PIN = 5;
int greenLed_PIN = 6;
int greenButton_PIN = 7;
int redLed_PIN = 8;
int redButton_PIN = 9;
int yellowLed_PIN = 10;
int yellowButton_PIN = 11;

int blue_lastState = LOW;
int blue_currentState;
int green_lastState = LOW;
int green_currentState;
int red_lastState = LOW;
int red_currentState;
int yellow_lastState = LOW;
int yellow_currentState;

String arduinoToDo;

void setup() {

  Serial.begin(9600);

  lc.shutdown(0, false);
  lc.setIntensity(0, 1);
  lc.clearDisplay(0);

  pinMode(blueLed_PIN, OUTPUT);
  pinMode(yellowLed_PIN, OUTPUT);
  pinMode(redLed_PIN, OUTPUT);
  pinMode(greenLed_PIN, OUTPUT);

  pinMode(blueButton_PIN, INPUT_PULLUP);
  pinMode(greenButton_PIN, INPUT_PULLUP);
  pinMode(redButton_PIN, INPUT_PULLUP);
  pinMode(yellowButton_PIN, INPUT_PULLUP);

  Serial.println("ready");
}

void pinUpDraw(byte columPixel[8], int delayTime) {
  lc.clearDisplay(0);
  for (int i = 0; i < 8; i++) {
    lc.setRow(0, i, columPixel[i]);
  }
  delay(delayTime);
}

void mouse() {
  lc.clearDisplay(0);
  byte mouse[8] = {B00000000, B11111100, B11111000, B01111111, B11111111, B11011111, B00001111, B00110000};
  pinUpDraw(mouse, 0);
}

void cat() {
  lc.clearDisplay(0);
  byte cat[8] = {B11100001, B01010110, B01111111, B01010111, B11100111, B00000011, B00000001, B00011110};
  pinUpDraw(cat, 0);
}

void blinkAllLed() {
  for (int i = 0; i < 10; i++) {
    digitalWrite(blueLed_PIN, HIGH);
    digitalWrite(greenLed_PIN, HIGH);
    digitalWrite(redLed_PIN, HIGH);
    digitalWrite(yellowLed_PIN, HIGH);
    delay(100);
    digitalWrite(blueLed_PIN, LOW);
    digitalWrite(greenLed_PIN, LOW);
    digitalWrite(redLed_PIN, LOW);
    digitalWrite(yellowLed_PIN, LOW);
    delay(100);
  }
  digitalWrite(blueLed_PIN, LOW);
  digitalWrite(greenLed_PIN, LOW);
  digitalWrite(redLed_PIN, LOW);
  digitalWrite(yellowLed_PIN, LOW);
}

String transformArduinoToDo(String stringToTransform, char separator, int index)  {
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = stringToTransform.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (stringToTransform.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? stringToTransform.substring(strIndex[0], strIndex[1]) : "";
}

void ledSwitch( String informationOnOffColorLed) {

  String onOffLed = transformArduinoToDo(informationOnOffColorLed, ':', 0);
  String colorLed = transformArduinoToDo(informationOnOffColorLed, ':', 1);

  if (onOffLed == "turnon") {
    if (colorLed == "green") {
      digitalWrite(greenLed_PIN, HIGH);
    }
    else if (colorLed == "red") {
      digitalWrite(redLed_PIN, HIGH);
    }

    else if (colorLed == "yellow") {
      digitalWrite(yellowLed_PIN, HIGH);
    }
    else if (colorLed == "blue") {
      digitalWrite(blueLed_PIN, HIGH);
    }
    else if (colorLed == "all") {
      digitalWrite(greenLed_PIN, HIGH);
      digitalWrite(redLed_PIN, HIGH);
      digitalWrite(yellowLed_PIN, HIGH);
      digitalWrite(blueLed_PIN, HIGH);
    }
  }

  else if (onOffLed == "turnoff") {
    if (colorLed == "green") {
      digitalWrite(greenLed_PIN, LOW);
    }
    else if (colorLed == "red") {
      digitalWrite(redLed_PIN, LOW);
    }
    else if (colorLed == "yellow") {
      digitalWrite(yellowLed_PIN, LOW);
    }
    else if (colorLed == "blue") {
      digitalWrite(blueLed_PIN, LOW);
    }
    else if (colorLed == "all") {
      digitalWrite(greenLed_PIN, LOW);
      digitalWrite(redLed_PIN, LOW);
      digitalWrite(yellowLed_PIN, LOW);
      digitalWrite(blueLed_PIN, LOW);
    }
  }
}

void ButtonPush() {
  blue_currentState = digitalRead(blueButton_PIN);
  if (blue_lastState == HIGH && blue_currentState == LOW) {
    Serial.println("button:blue");
  }
  blue_lastState = blue_currentState;

  green_currentState = digitalRead(greenButton_PIN);
  if (green_lastState == HIGH && green_currentState == LOW) {
    Serial.println("button:green");
  }
  green_lastState = green_currentState;

  red_currentState = digitalRead(redButton_PIN);
  if (red_lastState == HIGH && red_currentState == LOW) {
    Serial.println("button:red");
  }
  red_lastState = red_currentState;

  yellow_currentState = digitalRead(yellowButton_PIN);
  if (yellow_lastState == HIGH && yellow_currentState == LOW) {
    Serial.println("button:yellow");
  }
  yellow_lastState = yellow_currentState;
}

void clockTimer() {

  lc.clearDisplay(0);

  byte clockTime0[8] = {B00011100, B00100010, B01001001, B01011001, B01000001, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime0, 500);
  ledSwitch(arduinoToDo);
  ButtonPush();

  byte clockTime1[8] = {B00011100, B00100010, B01010101, B01001001, B01000001, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime1, 500);
  ledSwitch(arduinoToDo);
  ButtonPush();

  byte clockTime2[8] = {B00011100, B00100010, B01001001, B01001101, B01000001, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime2, 500);
  ledSwitch(arduinoToDo);
  ButtonPush();

  byte clockTime3[8] = {B00011100, B00100010, B01000001, B01001101, B01001001, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime3, 500);
  ledSwitch(arduinoToDo);
  ButtonPush();

  byte clockTime4[8] = {B00011100, B00100010, B01000001, B01001101, B01001001, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime4, 500);
  ledSwitch(arduinoToDo);
  ButtonPush();

  byte clockTime5[8] = {B00011100, B00100010, B01000001, B01001001, B01010101, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime5, 500);
  //ledSwitch(arduinoToDo);
  ButtonPush();

  byte clockTime6[8] = {B00011100, B00100010, B01000001, B01011001, B01001001, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime6, 500);
  ledSwitch(arduinoToDo);
  ButtonPush();

  byte clockTime7[8] = {B00011100, B00100010, B01010001, B01001001, B01010001, B00100010, B00011100, B00000000};
  pinUpDraw(clockTime7, 500);
  ledSwitch(arduinoToDo);
  ButtonPush();
}

void loop() {
  if (Serial.available()) {
    arduinoToDo = Serial.readString();
    arduinoToDo.trim();
    arduinoToDo.toLowerCase();

    if (arduinoToDo == "mouse") {
      mouse();
      blinkAllLed();
      delay(2000);
      lc.clearDisplay(0);
      exit(0);
    }
    else if (arduinoToDo == "cat") {
      cat();
      blinkAllLed();
      delay(2000);
      lc.clearDisplay(0);
      exit(0);
    }
  }
  clockTimer();
}
