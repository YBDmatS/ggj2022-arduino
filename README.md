# [[ WhiteSpace Arduino ]]

![intro](intro.png)

## Description

Developed during the Global Game Jam 2022, this is the **Arduino part (C++ language)** of the project.


## Story

Duality between cat and mice. The rocket destination is the planet “croquette” but laboratory mice (used by the cat to experiment alien food) managed to break free. The mice want to join the planet “gruyère”. The big cat uses a jet-pack to fly into the ship (he’s to big to walk) and has to kill all mice by splashing them. On the other side, the mice have to kill the cat by crashing his jetpack.

You can download and play this game for free: https://globalgamejam.org/2022/games/white-space-4


## API

The unity project communicates with the Arduino part through the **USB (Port-Serial)**.

<u>From Arduino to Unity</u>:

- ```c++
  button:blue // sent when the blue button has been pushed
  ```
- ```c++
  button:green // sent when the green button has been pushed
  ```
- ```c++
  button:yellow // sent when the yellow button has been pushed
  ```
- ```c++
  button:red // sent when the red button has been pushed
  ```

<u>From Unity to Arduino:</u>

- ```c++
  turnon:green // to turn on the green LED
  ```
- ```c++
  turnoff:green // to turn off the green LED
  ```
- ```c++
  turnon:red // to turn on the red LED
  ```
- ```c++
  turnoff:red // to turn off the red LED
  ```
- ```c++
  turnon:yellow // to turn on the yellow LED
  ```
- ```c++
  turnoff:yellow // to turn off the yellow LED
  ```
- ```c++
  turnon:blue // to turn on the blue LED
  ```
- ```c++
  turnoff:blue // to turn off the blue LED
  ```
- ```c++
  turnon:all // to turn on all LEDs
  ```
- ```c++
  turnoff:all // to turn off all LEDs
  ```
- ```c++
  cat // to display the animation when the cat player won
  ```
- ```c++
  mouse // to display the animation when the mice won
  ```
  
  
## Visual

![displayClock](displayClock.jpg)

![displayCat](displayCat.jpg)

![displayMouse](displayMouse.jpg)